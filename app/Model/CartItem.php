<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;

/**
 * Class CartItem
 * @package App\Model
 *
 * @property string cart_id
 * @property int product_id
 * @property int quantity
 * @property int total_price
 * @property int total_discount
 */
class CartItem extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'cart_id',
        'product_id',
        'quantity',
        'total_price',
        'total_discount'
    ];

    public $incrementing = false;
    protected $primaryKey = 'cart_id';

    /**
     * get cart relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function cart()
    {
        return $this->belongsTo(Cart::class);
    }


    /**
     * get product relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function product()
    {
        return $this->hasOne(Product::class);
    }


    /**
     * find cartItem with cart id and product id
     *
     * @param string $cart_id
     * @param int $product_id
     *
     * @return CartItem
     */
    public function findCartItem(string $cart_id, int $product_id)
    {
        return $this->where(['cart_id' => $cart_id, 'product_id' => $product_id])->first();
    }
}
